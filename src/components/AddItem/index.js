import React, {Component} from 'react';

import './style.css'


export default class AddItem extends Component{

    constructor() {
        super();

        this.state = {
            label: ""
        };

        this.onLabelChange = (e) => {
            this.setState({
                label: e.target.value
            })
        };

        this.onSubmit = (e) => {
            e.preventDefault();
            if(this.state.label.length)
                this.props.onAdded(this.state.label)
            this.setState({
                label: ""
            });
        }
    }



    render() {
        return (
            <form className="add-item d-flex"
                  onSubmit={this.onSubmit}
            >
                <input type="text"
                       className="form-control"
                       onChange={this.onLabelChange}
                       placeholder="What needs to be done"
                       value={this.state.label}

                />
                <button
                    type="submit"
                    className="btn btn-primary"
                >Add</button>
            </form>
        )
    }

}
