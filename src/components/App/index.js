import React, {Component} from 'react';

import AddItem from '../AddItem';
import AppHeader from '../AppHeader';
import SearchPanel from '../SearchPanel';
import TodoList from '../TodoList';
import ItemStatusFilter from '../ItemStatusFilter';

import './style.css';

export default class App extends Component {

    constructor() {
        super();

        this.id = 1;

        this.createTodoItem = (label) => ({
            label,
            important: false,
            done: false,
            id: this.id++
        });

        this.deleteItem = (id) => {
            this.setState(({todoData}) => {
                const newTodo = todoData.filter((item) => item.id !== id);
                return {
                    todoData: newTodo
                }
            });
        };

        this.addItem = (text) => {
            const newItem = this.createTodoItem(text);

            this.setState(({todoData}) => {

                const newArr = [
                    ...todoData,
                    newItem
                ];

                return {
                    todoData: newArr
                }
            });
        };

        function toggleProperty(arr, id, propName){
            const idx = arr.findIndex((el) => el.id === id);

            const oldItem = arr[idx];
            const newItem = {...oldItem, [propName]: !oldItem[propName]};
            const newArr = arr.map((item)=>item === oldItem ? newItem : item);
            return {
                todoData: newArr
            }
        }

        this.onToggleDone = (id) => {
            this.setState(({todoData}) => toggleProperty(todoData, id, "done"))
        };

        this.onToggleImportant = (id) => {
            this.setState(({todoData}) => toggleProperty(todoData, id, "important"))
        };

        this.search = (items, term) => {
            if(!term) return items;
            return items.filter(({label}) =>
                ~label.toLowerCase().indexOf(term.toLowerCase()))
        };

        this.filter = (items, filter) => {
            switch (filter) {
                case 'all':
                    return items;
                case 'active':
                    return items.filter(item => !item.done);
                case 'done':
                    return items.filter(item => item.done);
                default:
                    return items;

            }
        };

        this.onSearchChange = (term) => {
            this.setState({term});
        };

        this.onFilterChange = (filter) => {
            this.setState({filter});
        };



        this.state = {
            todoData: [
                this.createTodoItem("Drink Coffee"),
                this.createTodoItem("Build React App"),
                this.createTodoItem("Have a lunch")
            ],
            term: '',
            filter: 'all'
        };
    };

    render () {
        const {todoData, term, filter} = this.state;
        const visibleItem = this.filter(this.search(todoData, term), filter);
        const doneCount = todoData.filter((el) => el.done).length;
        const todoCount = todoData.length - doneCount;

        return (
            <div className="todo-app">
                <AppHeader toDo={todoCount} done={doneCount} />
                <div className="top-panel d-flex">
                    <SearchPanel
                        onSearchChange={this.onSearchChange}
                    />
                    <ItemStatusFilter
                        filter={filter}
                        onFilterChange={this.onFilterChange}
                    />
                </div>

                <TodoList
                    todos={visibleItem}
                    onDeleted={this.deleteItem}
                    onToggleDone={this.onToggleDone}
                    onToggleImportant={this.onToggleImportant}
                />
                <AddItem onAdded = {(text) => this.addItem(text)} />

            </div>
        );
    }

};

