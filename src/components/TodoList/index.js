import React from "react";
import TodoListItem from "../TodoListItem";

import './style.css'

const TodoList = ({
                    todos,
                    onDeleted,
                    onToggleDone,
                    onToggleImportant
                  }) => {

    const elements = todos.map(({id, ...itemProps})=>{
        return(

            <li className="list-group-item" key={id}>
                <TodoListItem
                    {...itemProps }
                    onDeleted={() => onDeleted(id)}
                    onToggleDone={() => onToggleDone(id)}
                    onToggleImportant={() => onToggleImportant(id)}
                />
            </li>

        );
    });

    return (
        <ul className="list-group todo-list">
            {elements}
        </ul>
    );
};

export default TodoList;